ansible role for lb-controller
==============================

Ansible role to install and configure [lb-controller](https://pypi.org/project/lb-controller/).

lb-controller is a kubernetes operator that can be used to implement
`LoadBalancer` services with an on premise cluster as it manages the
configuration of HAProxy instances to associate IPs (from a pool at your
disposal) and the corresponding node ports services in the cluster.

This can be integrated nicely within a kubespray project for instance.

TODO
----

* :warning: Define a service account dedicated to this task and stop using
  admin conf from kubespray!

Requirements
------------

None

Role Variables
--------------

Default variables:

    lb_controller_version: "0.0.2"
    # the full path to python to use in the systemd unit
    lb_controller_python_command: /usr/bin/python3
    # where to find the app after pip installing it
    lb_controller_path: /usr/local/lib/python3.6/dist-packages/lb_controller/main.py
    # the config dir
    lb_controller_config_dir: /etc/lb-controller
    # list of network ranges of load balancer IPs
    lb_controller_ip_nets:
      - 172.17.255.0/24

    lb_controller_interface: eth0

    # metrics
    lb_controller_metrics_addr: 127.0.0.1
    lb_controller_metrics_port: 8181

    # keepalived specific params
    lb_controller_keepalived_virtual_router_id: 54
    lb_controller_keepalived_auth_pass: 54545454


Example Playbook
----------------

    - hosts: haproxy
      become: yes
      roles:
        - role: lb_controller
          tags: lb_controller
      environment:
        http_proxy: "{{ http_proxy }}"
        https_proxy: "{{ https_proxy }}"

License
-------

MIT
